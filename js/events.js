function setEventListeners (state) {
    document.getElementById('change').addEventListener('click', onChangeClick)

    async function onChangeClick (event) {
        await changeMessage(state)
        updateUi(state)
    }

}