const api = {
    getRandomMessage: async function () {
        let response = await fetch(
            'https://api.chucknorris.io/jokes/random'
        )
        return (await response.json()).value
    }
}

async function changeMessage (state) {
    state.message = await api.getRandomMessage()
}

try {
    module.exports = { changeMessage, api }
} catch (error) {

}