const { changeMessage, api } = require('../domain.js')

test('changeMessage', async function () {
    api.getRandomMessage = jest.fn(async function () {
        return 'dos'
    })
    let state = { message: 'uno' }
    await changeMessage(state)
    expect(state.message).toBe('dos')
})